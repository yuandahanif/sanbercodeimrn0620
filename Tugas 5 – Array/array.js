// Soal No. 1 (Range) 
function range(startNum, finishNum) {
    if (typeof startNum != 'number' || typeof finishNum != 'number') { return -1 }
    
    let isAsc = startNum <= finishNum;
    let loop = true;
    let arrayResult = [];

    while (loop) {
        arrayResult.push(startNum);
        isAsc ? startNum++ : startNum--;
        // loop = !(startNum == finishNum);
        loop = isAsc ? startNum <= finishNum : startNum >= finishNum;
    }
    return arrayResult;
}
 // * Test-case
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log('\n');

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    if (typeof startNum != 'number' || typeof finishNum != 'number' || typeof step != 'number') { return -1 }
    
    let isAsc = startNum <= finishNum;
    let loop = true;
    let arrayResult = [];

    while (loop) {
        arrayResult.push(startNum);
        startNum =  isAsc ? startNum +=step : startNum -=step;
        // loop = !(startNum == finishNum);
        loop = isAsc ? startNum <= finishNum : startNum >= finishNum;
    }
    return arrayResult;
}
// * Test-case
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('\n');

// Soal No. 3 (Sum of Range)
function sum(startNum = 0, finishNum = 0, step = 1) {
    let arrayToSum = rangeWithStep(startNum, finishNum, step);
    let result = 0;
    for (let i = 0; i < arrayToSum.length; i++) {
        result += arrayToSum[i];
    }
    return result;
}
// * Test-case
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum(1,20,2)) // 100

console.log('\n');

// Soal No. 4 (Array Multidimensi)
function dataHandlig(arrayParam) {
    let result = new String();
    arrayParam.forEach( data => {
        result +=(`
            Nomor ID:  ${data[0]}
            Nama Lengkap:  ${data[1]}
            TTL:  ${data[2]} ${data[3]}
            Hobi:  ${data[4]}
        `);
    });
    return result;
}
// * Test-case
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
// console.log(dataHandlig(input));

// Soal No. 5 (Balik Kata)
function balikKata(kata) {
    let result = '';
    for (let i = kata.length-1; i >= 0 ; i--) {
        result += kata[i];
    }
    return result;
}
// * Test-case
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\n');

// Soal No. 6 (Metode Array)
function dataHandling2(arrayP) {

    let arrayParam = arrayP.slice();

    // nama
    let nama = arrayParam[1] += 'Elsharawy';
    nama = arrayParam[1].slice(0, 14);

    // alamat
    arrayParam[2] = `Provinsi ${arrayParam[2]}`;
    let alamat = arrayParam[2];

    // tanggal
    let tanggal = arrayParam[3].split('/');

    let tanggal_short = tanggal.slice();
    tanggal_short.sort( function (value1, value2) { return parseInt(value2) - parseInt(value1) } );

    let bulan = toBulan(parseInt(tanggal[1]));
    tanggal = tanggal.join('-');

    // hobi
    // kelamin
    // sekolah
    arrayParam.splice(4,1,'Pria','SMA Internasional Metro');

    console.log(arrayParam);
    console.log(bulan);
    console.log(tanggal_short);
    console.log(tanggal);
    console.log(nama);
    
}


function toBulan(bulan) {
    switch (bulan) {
        case 01:
            bulan = 'Januari';
            break;
        case 02:
            bulan = 'Februri';
            break;
        case 03:
            bulan = 'Maret';
            break;
        case 04:
            bulan = 'April';
            break;
        case 05:
            bulan = 'Mei';
            break;
        case 06:
            bulan = 'Juni';
            break;
        case 07:
            bulan = 'Juli';
            break;
        case 08:
            bulan = 'Agustus';
            break;
        case 09:
            bulan = 'September';
            break;
        case 10:
            bulan = 'Oktober';
            break;
        case 11:
            bulan = 'November';
            break;
        case 12:
            bulan = 'Desember';
            break;
    
        default:
            console.log('bulan disini dengan angka antara 1 - 12');
            break;
    }
    return bulan;
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 