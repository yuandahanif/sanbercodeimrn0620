// di index.js
let readBooks = require('./callback.js');

let books = [
  {
    name: 'LOTR',
    timeSpent: 3000,
  },
  {
    name: 'Fidas',
    timeSpent: 2000,
  },
  {
    name: 'Kalkulus',
    timeSpent: 4000,
  },
];

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], (sisaWaktu) => {
  readBooks(sisaWaktu, books[1], (sisaWaktu) => {
    readBooks(sisaWaktu, books[2], (time) => {
      console.log(`sisa waktu saya ${time}`);
    });
  });
});
