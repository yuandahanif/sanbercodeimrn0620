// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    arr.forEach(people => {
        let peopleObject = {};
        let now = new Date();
        let age = typeof people[3] == 'undefined' || people[3] > now.getFullYear() ? "Invalid Birth Year" : now.getFullYear() - people[3];
        if (typeof people[0] == 'undefined') {
            console.log("");
            return '';
        }
        peopleObject.firstName = people[0];
        peopleObject.lastName = people[1];
        peopleObject.gender = people[2];
        peopleObject.age = age;
        console.log(peopleObject);
    });
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log('\n');
// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    if ((typeof memberId == 'undefined' || memberId == '') || typeof money == 'undefined') {
        return '\nMohon maaf, toko X hanya berlaku untuk member saja\n';
    }
    const itemList = [{
            'itamName': 'Sepatu Stacattu',
            'price': 1500000,
        },
        {
            'itamName': 'Baju Zoro',
            'price': 500000,
        },
        {
            'itamName': 'Baju H&N',
            'price': 250000,
        },
        {
            'itamName': 'Sweater Uniklooh',
            'price': 175000,
        },
        {
            'itamName': 'Casing Handphone',
            'price': 50000,
        },
    ];
    let listPurchased = [];
    let changeMoney = money;

    itemList.forEach(item => {
        if (changeMoney >= item.price) {
            listPurchased.push(item.itamName);
            changeMoney -= item.price;
        }
    });

    let shopingResult = {
        memberId,
        money,
        listPurchased,
        changeMoney
    };
    return listPurchased.length == 0 ? 'Mohon maaf, uang tidak cukup' : shopingResult;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n');
// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang = []) {
    const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    const resultPenumpang = [];
    if (arrPenumpang.length == 0) {
        return [];
    }

    arrPenumpang.forEach(penumpang => {
        let tempPenumpang = {};
        let rute1 = rute.indexOf(penumpang[1]) + 1;
        let rute2 = rute.indexOf(penumpang[2]) + 1;
        let panjangRute = 0;

        tempPenumpang.penumpang = penumpang[0];
        tempPenumpang.naikDari = penumpang[1];
        tempPenumpang.tujuan = penumpang[2];

        panjangRute = rute1 > rute2 ? rute1 - rute2 : rute2 - rute1;
        tempPenumpang.bayar = panjangRute * 2000;

        resultPenumpang.push(tempPenumpang);
    })
    return resultPenumpang;

}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B'],
    ['penumpang 3', 'D', 'A']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 },
//  { penumpang: 'penumpang C', naikDari: 'D', tujuan: 'A', bayar: 6000 }]

console.log(naikAngkot([])); //[]