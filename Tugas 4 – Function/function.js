// No. 1 
function teriak() {
    return "Halo Sanbers!";
}
console.log(teriak()) // "Halo Sanbers!" 

console.log('\n');


// No. 2 
const kalikan = (n1, n2) => n1 * n2;
 
let num1 = 12
let num2 = 4
 
let hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log('\n');

// No. 3 
function introduce(...me) {
    return `Nama saya ${me[0]}, umur saya ${me[1]} tahun, alamat saya di ${me[2]}, dan saya punya hobby yaitu ${me[3]}!`;
}
 
let name = "Agus"
let age = 30
let address = "Jln. Malioboro, Yogyakarta"
let hobby = "Gaming"
 
let perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 