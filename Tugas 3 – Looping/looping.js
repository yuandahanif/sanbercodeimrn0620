// No. 1 Looping While

console.log('LOOPING PERTAMA');
let iPertama = 1;
while (iPertama < 20) {
    console.log(`${iPertama + 1} - I love coding`);
    iPertama++;
}

console.log('\nLOOPING KEDUA');
let iKedua = 20;
while (iKedua > 1) {
    console.log(`${iKedua} - I will become a mobile developer`);
    iKedua--;
}

console.log('\n');
// No. 2 Looping menggunakan for
for (let i = 1; i <= 20; i++) {
    let angkaGAnjil = i % 2 !== 0;

    if (angkaGAnjil && i % 3 != 0) {
        console.log(`${i} - Santai`);
    }else if (angkaGAnjil && i % 3 == 0) {
        console.log(`${i} - I Love Coding `);
    }else{
        console.log(`${i} - Berkualitas`);
    }
}

console.log('\n');
// No. 3 Membuat Persegi Panjang
for (let i = 0; i < 4; i++) {
    // * jawaban 1
    console.log('########');

    // * jawban 2
    // let satuBaris = '';
    // for (let j = 0; j < 8; j++) {
    //     satuBaris += '#';
    // }
    // console.log(satuBaris);
}

console.log('\n');
//No. 4 Membuat Tangga  
let pyramidBlock = '#';
for (let i = 0; i < 7; i++) {
    console.log(pyramidBlock);
    pyramidBlock += '#';
}

console.log('\n');
// No. 5 Membuat Papan Catur
for (let i = 0; i < 8; i++) {
    let putih = i % 2 == 0;
    let satuBaris = '';

    for (let j = 0; j < 8; j++) {
        satuBaris += putih ? ' ' : '#';
        putih = !putih;
    }
    
    console.log( satuBaris );
}