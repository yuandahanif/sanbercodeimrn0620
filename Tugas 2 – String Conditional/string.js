// * maaf sebelumnya, saya menggunakan let bukan var.
// Soal No. 1 (Membuat kalimat)
let world = 'JavaScript';
let second = 'is';
let third = 'awsome';
let fourth = 'and';
let fifth = 'I';
let sixth = 'love';
let seventh = 'it!';

let jawaban1 = world+' '+second+' '+third+' '+fourth+' '+fifth+' '+sixth+' '+seventh;
let jawaban2 = world.concat(' '.concat(second)).concat(' '.concat(third))
                .concat(' '.concat(fourth))
                .concat(' '.concat(fifth))
                .concat(' ').concat(seventh);
let jawaban3 = `${world} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`;

console.log(jawaban3);

console.log('\n');

// Soal No.2 Mengurai kalimat (Akses karakter dalam string), 
let sentence = "I am going to be React Native Developer"; 
let exampleFirstWord = sentence[0] ; 
let secondWord = sentence[2] + sentence[3]  ; 
let thirdWord = sentence[5] + sentence[6]  + sentence[7]  + sentence[8]  + sentence[9]; 
let fourthWord = sentence[11] + sentence[12]; 
let fifthWord = sentence[14]  + sentence[15]; 
let sixthWord = sentence[17]  + sentence[18]  + sentence[19]  + sentence[20]  + sentence[21]; 
let seventhWord = sentence[30]  + sentence[31]  + sentence[32]  + sentence[33]  + sentence[34]  + sentence[35]  + sentence[36]  + sentence[37]  + sentence[39-1];
console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 

console.log('\n');


// Soal No. 3 Mengurai Kalimat (Substring) 
let sentence2 = 'wow JavaScript is so cool'; 
let exampleFirstWord2 = sentence2.substring(0, 3); 
let secondWord2 = sentence2.substring(4, sentence2.indexOf(' ', 5));
let thirdWord2 = sentence2.substring(15, sentence2.indexOf(' ', 16));
let fourthWord2 = sentence2.substring(18, sentence2.indexOf(' ', 19));
let fifthWord2 = sentence2.substring(21 ,sentence2.length );
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log('\n');

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String

let sentence3 = 'wow JavaScript is so cool'; 
let exampleFirstWord3 = sentence3.substring(0, 3); 
let secondWord3 = sentence3.substring(4 ,sentence3.indexOf(' ' ,5));
let thirdWord3 = sentence3.substring(15 ,sentence3.indexOf(' ' ,16));
let fourthWord3 = sentence3.substring(18 ,sentence3.indexOf(' ' ,19));
let fifthWord3 = sentence3.substring(21 ,sentence3.length );

let firstWordLength = exampleFirstWord3.length;
let secondWordLength = secondWord3.length;  
let thirdWordLength = thirdWord3.length;  
let fourthWordLength = fourthWord3.length;  
let fifthWordLength = fifthWord3.length;  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 